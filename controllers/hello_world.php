<?php

/**
 * Hello World controller.
 *
 * @category   Apps
 * @package    Hello_World
 * @subpackage Views
 * @author     Your name <your@e-mail>
 * @copyright  2013 Your name / Company
 * @license    Your license
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Hello World controller.
 *
 * @category   Apps
 * @package    Hello_World
 * @subpackage Controllers
 * @author     Your name <your@e-mail>
 * @copyright  2013 Your name / Company
 * @license    Your license
 */

class Hello_world extends ClearOS_Controller
{
    /**
     * Hello World default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('hello_world');

        // Load views
        //-----------

        $this->page->view_form('hello_world', NULL, lang('hello_world_app_name'));
    }
}
